/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef IGRAPHICSTREEVIEW_H
#define IGRAPHICSTREEVIEW_H

#include <QtCore/QRectF>
#include <QtCore/QPoint>
#include <QtCore/QPointF>
#include <QtCore/QModelIndex>

#include <qglobal.h>

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QGraphicsView>
#else
#include <QtWidgets/QGraphicsView>
#endif

#include <ksoftvis_export.h>

class QAbstractItemModel;

class KSOFTVIS_EXPORT IGraphicsTreeView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit IGraphicsTreeView(QWidget *parent = nullptr);
    virtual ~IGraphicsTreeView();

    virtual void setTreeModel(QAbstractItemModel *treeModel, const QModelIndex &treeModelRootIndex = QModelIndex());

public Q_SLOTS:
    virtual void updateTreeScene() = 0;
    void resetZoom();
    void resetPan();

private Q_SLOTS:
    void sceneRectChanged(const QRectF &rect);

protected:
    // Navigation-related attributes
    QPointF m_delta;
    QPointF m_lastDelta;
    QPoint  m_lastPanPoint;
    QRectF  m_currentSceneRect;

    // Navigation-related methods
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void wheelEvent(QWheelEvent *event) override;
    virtual void paintEvent(QPaintEvent *event) override;

    // The scene
    QGraphicsScene *m_scene;

    // The tree model and root index
    QAbstractItemModel *m_treeModel;
    QModelIndex m_treeModelRootIndex;

    void reset();
};

#endif // IGRAPHICSTREEVIEW_H
