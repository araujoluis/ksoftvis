/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef IGRAPHICSCOMPOUNDTREEVIEW_H
#define IGRAPHICSCOMPOUNDTREEVIEW_H

#include "igraphicstreeview.h"

#include <ksoftvis_export.h>

class KSOFTVIS_EXPORT IGraphicsCompoundTreeView : public IGraphicsTreeView
{
    Q_OBJECT

public:
    IGraphicsCompoundTreeView(QWidget *parent = nullptr);
    virtual ~IGraphicsCompoundTreeView();

    virtual void setAdjacencyModel(QAbstractItemModel *adjacencyModel);

public Q_SLOTS:
    virtual void updateTreeScene() override = 0;
    virtual void updateAdjacencyScene() = 0;

protected:
    // The adjacency model
    QAbstractItemModel *m_adjacencyModel;
};

#endif // INODELINKGRAPHICSTREEVIEW_H
