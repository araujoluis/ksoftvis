/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "igraphicstreeview.h"

#include <QtGui/QMouseEvent>
#include <QtGui/QWheelEvent>

IGraphicsTreeView::IGraphicsTreeView(QWidget *parent) :
    QGraphicsView(parent),
    m_scene(new QGraphicsScene(this)),
    m_treeModel(nullptr),
    m_treeModelRootIndex(QModelIndex())
{
    setScene(m_scene);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);

    connect(m_scene, SIGNAL(sceneRectChanged(QRectF)), SLOT(sceneRectChanged(QRectF)));
}

IGraphicsTreeView::~IGraphicsTreeView()
{
}

void IGraphicsTreeView::setTreeModel(QAbstractItemModel *treeModel, const QModelIndex &treeModelRootIndex)
{
    if (m_treeModel == treeModel)
        return;

    m_treeModel = treeModel;
    if (treeModelRootIndex.isValid() && treeModelRootIndex != QModelIndex())
    {
        m_treeModelRootIndex = treeModelRootIndex;
    }
    else
    {
        if (m_treeModel)
            m_treeModelRootIndex = m_treeModel->index(0, 0, QModelIndex());
        else
            m_treeModelRootIndex = QModelIndex();
    }

    reset();
}

void IGraphicsTreeView::resetZoom()
{
    m_currentSceneRect = sceneRect();
}

void IGraphicsTreeView::resetPan()
{
    m_delta = QPointF();
}

void IGraphicsTreeView::sceneRectChanged(const QRectF &rect)
{
    m_currentSceneRect = rect;
}

void IGraphicsTreeView::mousePressEvent(QMouseEvent *event)
{
    m_lastPanPoint = event->pos();
}

void IGraphicsTreeView::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    m_lastPanPoint = QPoint();
    m_lastDelta = m_delta;
}

void IGraphicsTreeView::mouseMoveEvent(QMouseEvent *event)
{
    if(!m_lastPanPoint.isNull())
        m_delta = m_lastDelta + mapToScene(m_lastPanPoint) - mapToScene(event->pos());
}

void IGraphicsTreeView::wheelEvent(QWheelEvent *event)
{
    if(!event->angleDelta().isNull())
        m_currentSceneRect = QRectF(m_currentSceneRect.x()+m_currentSceneRect.width()*0.05,
                                    m_currentSceneRect.y()+m_currentSceneRect.height()*0.05,
                                    m_currentSceneRect.width()-m_currentSceneRect.width()*0.1,
                                    m_currentSceneRect.height()-m_currentSceneRect.height()*0.1);
    else
        m_currentSceneRect = QRectF(m_currentSceneRect.x()-m_currentSceneRect.width()*0.05,
                                    m_currentSceneRect.y()-m_currentSceneRect.height()*0.05,
                                    m_currentSceneRect.width()+m_currentSceneRect.width()*0.1,
                                    m_currentSceneRect.height()+m_currentSceneRect.height()*0.1);
}

void IGraphicsTreeView::paintEvent(QPaintEvent *event)
{
    fitInView(m_currentSceneRect.x()+m_delta.x(), m_currentSceneRect.y()+m_delta.y(),
              m_currentSceneRect.width(), m_currentSceneRect.height(), Qt::KeepAspectRatio);
    QGraphicsView::paintEvent(event);
}

void IGraphicsTreeView::reset()
{
    m_scene->clear();
    m_scene->setSceneRect(QRectF(0, 0, 0, 0));
    resetZoom();
    resetPan();
}
