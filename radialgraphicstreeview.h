/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef RADIALGRAPHICSTREEVIEW_H
#define RADIALGRAPHICSTREEVIEW_H

#include <QtCore/QFlags>

#include "igraphicscompoundtreeview.h"

class KSOFTVIS_EXPORT RadialGraphicsTreeView : public IGraphicsCompoundTreeView
{
    Q_OBJECT

public:
    enum ViewFlag
    {
        ShowNormalRadialViewNode            = 0x1,
        ShowNormalRadialViewEdge            = 0x2,
        ShowNormalRadialViewLevelCircles    = 0x4,
        ShowMirroredRadialView              = 0x8,
        ShowAdjacencyModel                  = 0x10
    };
    Q_DECLARE_FLAGS(ViewFlags, ViewFlag)

    RadialGraphicsTreeView(QWidget *parent = nullptr);
    virtual ~RadialGraphicsTreeView();

    void setViewFlag(ViewFlag viewFlag, bool enabled = true);
    void setViewFlags(ViewFlags viewFlags);

public Q_SLOTS:
    void setNodeRadius(qreal nodeRadius);
    void setNodePen(const QPen &nodePen);
    void setNodeBrush(const QBrush &nodeBrush);
    void setEdgePen(const QPen &edgePen);
    void setInterLevelSpacing(qreal interLevelSpacing);
    void setInterSpanSpacing(qreal interSpanSpacing);

    void setMirroredRadiusIncrementFactor(qreal mirroredRadiusIncrementFactor);

    void setBundlingStrengthFactor(qreal bundlingStrengthFactor);
    void setAdjacencyEdgePenWidth(qreal adjacencyEdgePenWidth);

    virtual void updateTreeScene() override;
    virtual void updateAdjacencyScene() override;

protected:
    // View properties
    ViewFlags m_viewFlags;

    // Radial layout properties
    qreal  m_nodeRadius;
    QPen   m_nodePen;
    QBrush m_nodeBrush;
    QPen   m_edgePen;
    qreal  m_interLevelSpacing;
    qreal  m_interSpanSpacing;

    // Mirrored radial layout properties
    qreal m_mirroredRadiusIncrementFactor;

    // Adjacency edge bundling properties
    qreal m_bundlingStrengthFactor;
    qreal m_adjacencyEdgePenWidth;

private:
    // Helper attributes for mirrored radial layout algorithm
    qreal m_maxRadius;
    typedef QPair<qreal, qreal> ItemSpan;
    QHash<QGraphicsItem *, ItemSpan> m_spanOf;
    QMultiHash<QGraphicsItem *, qreal> m_radiusOf;
    QHash<QGraphicsItem *, QPointF> m_mirroringOf;
    QMultiHash<QGraphicsItem *, QGraphicsItem *> m_childrenOf;
    qreal m_mirroredInterLevelSpacing;

    // Helper attributes for adjacency edge bundling drawing
    QList<QGraphicsPathItem *> m_pathItems;

    // Helper attributes for dummy adjacency model generation
    typedef QPair<QModelIndex, QModelIndex> ModelIndexPair;
    QHash<QModelIndex, QGraphicsItem *> m_itemForIndex;

    // Radial layout helper functions
    void updateTreeSceneStep(const QModelIndex &index, qreal radius, qreal startAngle, qreal spanAngle, QGraphicsItem *parentItem = nullptr, int maxLevel = -1);
    int subTreeSize(const QModelIndex &index) const;

    // Mirrored radial layout helper functions
    void buildMirroredTreeSceneStep(QGraphicsItem *originalItem, qreal mirroredRadius);

    // Adjacency edge bundling drawing helper functions
    QPainterPath lcaPathBetween(QModelIndex source, QModelIndex target) const;
    QList<QModelIndex> pathFromRootTo(QModelIndex index) const;
    void addPathFromLcaTo(const QList<QModelIndex> &fromLca, const QPointF &lcaProjectedPoint, QPainterPath *path) const;
    QPointF interpolate (QPointF a, QPointF b, qreal value) const;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(RadialGraphicsTreeView::ViewFlags)

#endif // RADIALGRAPHICSTREEVIEW_H
