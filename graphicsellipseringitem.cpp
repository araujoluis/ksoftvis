/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "graphicsellipseringitem.h"

#include <QtCore/qmath.h>

#include <QtGui/QPainter>

#define PI 3.14159265358979

static QPainterPath qt_graphicsItem_shapeFromPath(const QPainterPath &path, const QPen &pen)
{
    // We unfortunately need this hack as QPainterPathStroker will set a width of 1.0
    // if we pass a value of 0.0 to QPainterPathStroker::setWidth()
    const qreal penWidthZero = qreal(0.00000001);

    if (path == QPainterPath())
        return path;
    QPainterPathStroker ps;
    ps.setCapStyle(pen.capStyle());
    if (pen.widthF() <= 0.0)
        ps.setWidth(penWidthZero);
    else
        ps.setWidth(pen.widthF());
    ps.setJoinStyle(pen.joinStyle());
    ps.setMiterLimit(pen.miterLimit());
    QPainterPath p = ps.createStroke(path);
    p.addPath(path);
    return p;
}

GraphicsEllipseRingItem::GraphicsEllipseRingItem(qreal x, qreal y, qreal width, qreal height,
                                                 int startAngle, int spanAngle,
                                                 qreal ringFactor,
                                                 QGraphicsItem *parent) :
    QGraphicsEllipseItem(x, y, width, height, parent),
    m_ringFactor(qMin(qMax(ringFactor, 0.0), 1.0))
{
    setStartAngle(startAngle);
    setSpanAngle(spanAngle);
    setAcceptHoverEvents(true);
}

GraphicsEllipseRingItem::~GraphicsEllipseRingItem()
{
}

void GraphicsEllipseRingItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->setPen(pen());
    painter->setBrush(brush());
    painter->drawPath(buildPainterPath());
}

QPainterPath GraphicsEllipseRingItem::shape() const
{
    return qt_graphicsItem_shapeFromPath(buildPainterPath(), pen());
}

QRectF GraphicsEllipseRingItem::boundingRect() const
{
    return shape().controlPointRect();
}

qreal GraphicsEllipseRingItem::ringFactor() const
{
    return m_ringFactor;
}

void GraphicsEllipseRingItem::setRingFactor(qreal ringFactor)
{
    m_ringFactor = qMin(qMax(ringFactor, 0.0), 1.0);
}

void GraphicsEllipseRingItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event)
    QBrush itemBrush = brush();
    QColor itemColor = itemBrush.color();
    itemColor.setHsv(itemColor.hue(), itemColor.saturation(), itemColor.value()*1.5);
    itemBrush.setColor(itemColor);
    setBrush(itemBrush);
}

void GraphicsEllipseRingItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event)
    QBrush itemBrush = brush();
    QColor itemColor = itemBrush.color();
    itemColor.setHsv(itemColor.hue(), itemColor.saturation(), itemColor.value()/1.5);
    itemBrush.setColor(itemColor);
    setBrush(itemBrush);
}

QPainterPath GraphicsEllipseRingItem::buildPainterPath() const
{
    QPainterPath path;

    QRectF originalRect = rect();
    QRectF scaledRect (originalRect.x()*(1-m_ringFactor), originalRect.y()*(1-m_ringFactor), originalRect.width()*(1-m_ringFactor), originalRect.height()*(1-m_ringFactor));
    qreal a = originalRect.width()/2.0;
    qreal b = originalRect.height()/2.0;
    qreal scaledA = scaledRect.width()/2.0;
    qreal scaledB = scaledRect.height()/2.0;
    qreal startDegreeAngle = startAngle()/16.0;
    qreal stopDegreeAngle = startDegreeAngle + spanAngle()/16.0;
    qreal startRadAngle = (startDegreeAngle*PI)/180.0;
    qreal stopRadAngle = (stopDegreeAngle*PI)/180.0;
    qreal cosStartRadAngle = qCos(startRadAngle);
    qreal sinStartRadAngle = qSin(startRadAngle);
    qreal cosStopRadAngle = qCos(stopRadAngle);
    qreal sinStopRadAngle = qSin(stopRadAngle);
    qreal startRadAngleRadius = (a*b)/(qSqrt(qPow(b*cosStartRadAngle, 2)+qPow(a*sinStartRadAngle, 2)));
    qreal stopScaledRadAngleRadius = (scaledA*scaledB)/(qSqrt(qPow(scaledB*cosStopRadAngle, 2)+qPow(scaledA*sinStopRadAngle, 2)));

    path.moveTo(startRadAngleRadius*cosStartRadAngle, -startRadAngleRadius*sinStartRadAngle);
    path.arcTo(originalRect, startDegreeAngle, spanAngle()/16.0);
    path.lineTo(stopScaledRadAngleRadius*cosStopRadAngle, -stopScaledRadAngleRadius*sinStopRadAngle);
    path.arcTo(scaledRect, stopDegreeAngle, -spanAngle()/16.0);
    path.closeSubpath();

    return path;
}
